import subprocess

#Run trufflehog and get output https://janakiev.com/blog/python-shell-commands/
def runTrufflehog(repos):
	#Each index is the output of the couterpart repo in repo.txt
	output = []	
	count = len(repos)
	for i in range(count - 1): #For each repo
		#Run command
		process = subprocess.Popen(['trufflehog',  '--regex', '--entropy=False', 'https://github.com/dxa4481/truffleHog.git'], 
			stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = process.communicate() #output, error
		output.append(stdout)
	return output #keys


#Read Repositories from txt
def readRepositories():
	file = open('repo.txt', 'r')
	lines = []
	for line in file:
		lines.append(line)

	file.close()
	return lines

repos = readRepositories()
for repo in repos:
	print(repo)
runTrufflehog(repos)
